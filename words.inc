%include "list_config.inc"

%macro add_key 1
%if key_size = 8
    db %1, 0
%elif key_size = 16
    dw %1, 0
%else
    dd %1, 0
%endif
%endmacro

global list_start
global list_end
    
list_start:

colon "Salam", key_1
add_key "Hi"

colon "Inchi", key_2
add_key "What?"

colon "Mara kum", key_3
add_key "How are you?"

list_end:

